from setuptools import setup, find_packages

VERSION = "0.3"
DESCRIPTION = "Command line DRM downloader and extractor"
LONG_DESC = open("./README.md", "r").read()
KEYWORDS = "dl downloader drm"
CLASSIFIERS = [
    "Programming Language :: Python :: 3",
    "Operating System :: OS Independent",
]
setup(
    name="drm-dl",
    version=VERSION,
    description=DESCRIPTION,
    long_description=LONG_DESC,
    long_description_content_type="text/markdown",
    classifiers=CLASSIFIERS,
    keywords=KEYWORDS,
    packages=find_packages(),
    py_modules=["key_extractor", "drm_dl", "drm_web_dl"],
    entry_points={
        "console_scripts": [
            "drm-dl=drm_dl:main",
            "key-extractor=key_extractor:main",
            "drm-web-dl=drm_web_dl:main",
        ]
    },
    install_requires=["requests", "yt-dlp"],
    zip_safe=True,
)
