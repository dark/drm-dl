"""
Command line utility and python module to download drm videos.
"""
from __future__ import annotations
import argparse
import os
import shutil
import key_extractor
from yt_dlp import YoutubeDL
from subprocess import getstatusoutput


def download_video_from_keys(
    mpd_url: str, keys: list[str] = [], filename: str = "output.mp4"
):
    """
    Download drm video from keys list
    """
    dirname = os.path.dirname(filename)
    if dirname:
        os.makedirs(dirname, exist_ok=True)
    ytdl_opts = {
        "nocheckcertificate": True,
        "no_warnings": True,
        "allow_unplayable_formats": True,
        "fixup": "never",
    }
    if shutil.which("aria2c"):
        ytdl_opts.update(
            {
                "external_downloader": "aria2c",
                "external_downloader_args": {"aria2c": ["-x", "16", "-j", "32"]},
            }
        )
    encrypted_audio = "encrypted.m4a"
    encrypted_video = "encrypted.mp4"
    decrypted_audio = "decrypted.m4a"
    decrypted_video = "decrypted.mp4"

    if not keys:
        # TODO: NON DRM
        return

    ytdl_opts.update({"format": "ba", "outtmpl": encrypted_audio})
    with YoutubeDL(ytdl_opts) as ytdl:
        ytdl.download(mpd_url)

    ytdl_opts.update({"format": "bv", "outtmpl": encrypted_video})
    with YoutubeDL(ytdl_opts) as ytdl:
        ytdl.download(mpd_url)

    keys_string = key_extractor.get_string_from_keys(keys)
    st1, out1 = getstatusoutput(
        f"mp4decrypt {encrypted_audio} {decrypted_audio} {keys_string}"
    )
    os.remove(encrypted_audio)
    st2, out2 = getstatusoutput(
        f"mp4decrypt {encrypted_video} {decrypted_video} {keys_string}"
    )
    os.remove(encrypted_video)
    if st1:
        print(out1)
        return
    if st2:
        print(out2)
        return
    st3, out3 = getstatusoutput(
        f'ffmpeg -y -i {decrypted_audio} -i {decrypted_video} -c copy "{filename}"'
    )
    os.remove(decrypted_audio)
    os.remove(decrypted_video)
    if st3:
        print(out3)
        return
    return filename


def download_video_from_license(
    mpd_url: str, license_url: str, filename: str = "output.mp4"
):
    """
    Download drm video from license_url
    """
    keys = key_extractor.get_keys(mpd_url, license_url)
    if not keys:
        return
    downloaded = download_video_from_keys(mpd_url, keys, filename)
    return downloaded


def main():
    """
    Run from command line
    """
    parser = argparse.ArgumentParser(
        description="Download drm videos", allow_abbrev=True
    )
    parser.add_argument("mpd_url", type=str, help="Either mpd(dash) url or pssh key")
    parser.add_argument("license_url", type=str, help="License url for the mpd url")
    parser.add_argument(
        "--output",
        "-o",
        dest="filename",
        type=str,
        help="Filename for downloaded video. Default is output.mp4",
        default="output.mp4",
        required=False,
    )
    args = parser.parse_args()
    downloaded = download_video_from_license(
        args.mpd_url, args.license_url, args.filename
    )
    if not downloaded:
        parser.error("Download error.")
    else:
        parser.exit(0, f"Successfully downloaded, {args.mpd_url}: {args.filename}")


if __name__ == "__main__":
    main()
