"""
Command line utility and python module to extract drm keys.
"""
from __future__ import annotations
import argparse
import json
import re
import os
import requests


WV_API = "http://getwvkeys.cc/api"


def get_pssh(mpd_url: str, headers: dict = {}):
    """
    Get pssh from mpd url
    headers: Headers if needed
    """
    try:
        mpd_res = requests.get(mpd_url, headers)
    except:
        return
    try:
        matches = re.finditer("<cenc:pssh>(.*)</cenc:pssh>", mpd_res.text)
        pssh = next(matches).group(1)
    except:
        return
    return pssh


def get_keys(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
):
    """
    Get DRM keys from mpd_url and license url.
    proxy: proxy if needed
    buildinfo: CDM BLOB BUILD ID
    cache: True, to cache the mpd_url key, default False
    """
    pssh = get_pssh(mpd_url, headers)
    if not pssh:
        return
    data = {
        "license": license_url,
        "pssh": pssh,
        "proxy": proxy,
        "buildinfo": buildinfo,
        "cache": cache,
    }
    key_res = requests.post(WV_API, data=json.dumps(data))
    try:
        keys = key_res.json()["keys"]
    except:
        return
    keys = [key["key"] for key in keys]
    keys: list[str]
    return keys


def dump_keys(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
):
    """
    Dump keys on stdout
    """
    keys = get_keys(mpd_url, license_url, proxy, buildinfo, cache, headers)
    if not keys:
        return
    for key in keys:
        print(key)
    return keys


def get_string_from_keys(keys: list[str]):
    """
    Return keys string from keys list
    """
    keys_ls = []
    for key in keys:
        keys_ls.append(f"--key {key}")

    return " ".join(keys_ls)


def get_keys_string(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
):
    """
    Return keys string for mp4decrypt
    """
    keys = get_keys(mpd_url, license_url, proxy, buildinfo, cache, headers)
    if not keys:
        return
    return get_string_from_keys(keys)


def print_keys_string(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
):
    """
    Print keys string for mp4decrypt
    """
    keys_string = get_keys_string(
        mpd_url, license_url, proxy, buildinfo, cache, headers
    )
    if not keys_string:
        return
    print(keys_string)
    return keys_string


def get_keys_dict(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
):
    """
    Return keys dict
    """
    keys = get_keys(mpd_url, license_url, proxy, buildinfo, cache, headers)
    if not keys:
        return
    keys_dict = {
        "mpd_url": mpd_url,
        "keys": [{"kid": x[0], "key": x[1]} for x in [y.split(":") for y in keys]],
    }
    return keys_dict


def dump_keys_json(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
):
    """
    Dump keys json
    """
    keys_dict = get_keys_dict(mpd_url, license_url, proxy, buildinfo, cache, headers)
    if not keys_dict:
        return
    print(json.dumps(keys_dict))
    return keys_dict


def write_keys_json(
    mpd_url: str,
    license_url: str,
    proxy: str = "",
    buildinfo: str = "",
    cache: bool = False,
    headers: dict = {},
    filename: str = "keys.json",
):
    """
    Write keys in a json file
    """
    keys_dict = get_keys_dict(mpd_url, license_url, proxy, buildinfo, cache, headers)
    if not keys_dict:
        return
    dirname = os.path.dirname(filename)
    if dirname:
        os.makedirs(dirname, exist_ok=True)
    with open(filename, "w") as f:
        f.write(json.dumps(keys_dict))
        return keys_dict


def main():
    """
    Run from command line
    """
    parser = argparse.ArgumentParser(
        description="Exctract dash(mpd) drm keys", allow_abbrev=True
    )
    parser.add_argument("mpd_url", type=str, help="Either mpd(dash) url or pssh key")
    parser.add_argument("license_url", type=str, help="License url for the mpd url")
    parser.add_argument(
        "--proxy",
        "-p",
        type=str,
        help="Proxy url if needed",
        default="",
        required=False,
    )
    parser.add_argument(
        "--buildinfo",
        "-b",
        type=str,
        help="Buildinfo in this format: 'CDM BLOB BUILD ID'",
        default="",
        required=False,
    )
    parser.add_argument(
        "--cache",
        "-c",
        action="store_true",
        help="Cache the pssh key",
        required=False,
    )
    parser.add_argument(
        "--headers",
        "-H",
        type=str,
        help='Pass headers as \'{"a": 1, "b": 2}\' if required',
        default="{}",
        required=False,
    )
    parser.add_argument(
        "--get-string",
        "-g",
        action="store_true",
        help="Get keys string for mp4decrypt",
        required=False,
    )
    parser.add_argument(
        "--dump-json",
        "-j",
        action="store_true",
        help="Print json of keys",
        required=False,
    )
    parser.add_argument(
        "--write-json",
        "-w",
        dest="filename",
        help="Store keys in json file",
        required=False,
    )
    args = parser.parse_args()
    mpd_url = args.mpd_url
    license_url = args.license_url
    proxy = args.proxy
    buildinfo = args.buildinfo
    cache = args.cache
    mpd_url = mpd_url
    headers = json.loads(args.headers)
    mpd_args = (mpd_url, license_url, proxy, buildinfo, cache, headers)
    if args.get_string:
        keys = print_keys_string(*mpd_args)
    elif args.dump_json:
        keys = dump_keys_json(*mpd_args)
    elif args.filename:
        keys = write_keys_json(*mpd_args, args.filename)
    else:
        keys = dump_keys(*mpd_args)
    if not keys:
        parser.error("Wrong mpd_url or license")


if __name__ == "__main__":
    main()
