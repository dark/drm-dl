"""
Command line utility and python module to download drm videos from website.
"""
import re
import drm_dl
import argparse
import requests


def get_mpd_license(webpage: str):
    """
    Returns mpd and license from mpd
    """
    mpd_url = re.findall('<source src="(.*)">', webpage)[0]
    license_url = re.findall('"serverURL": "(.*)"', webpage)[0]
    return mpd_url, license_url


def get_mpd_license_from_url(url: str):
    """
    Returns mpd and license from url
    """
    webpage = requests.get(url).text
    return get_mpd_license(webpage)


def download_from_url(url: str, filename: str = "output.mp4"):
    """
    Download DRM videos from url
    """
    mpd_url, license_url = get_mpd_license_from_url(url)
    downloaded = drm_dl.download_video_from_license(mpd_url, license_url, filename)
    return downloaded


def main():
    """
    Run from command line
    """
    parser = argparse.ArgumentParser(
        description="Download drm videos from url", allow_abbrev=True
    )
    parser.add_argument("url", type=str, help="Url for drm video")
    parser.add_argument(
        "--output",
        "-o",
        dest="filename",
        type=str,
        help="Filename for downloaded video. Default is output.mp4",
        default="output.mp4",
        required=False,
    )
    args = parser.parse_args()
    downloaded = download_from_url(args.url, args.filename)
    if not downloaded:
        parser.error("Download error.")
    else:
        parser.exit(0, f"Successfully downloaded, {args.url}: {args.filename}")


if __name__ == "__main__":
    main()
